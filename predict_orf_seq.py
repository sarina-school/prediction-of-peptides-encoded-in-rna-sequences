from find_longest_orf import find_longest_orf
from translate_dna import translate_dna

def predict_orf_seq(sequence):
    """Predicts peptide sequence for longest ORF in DNA sequence.

    Finds the longest ORF within a DNA sequence and, if available,
    translates it to the peptide sequence (1-letter code) it encodes.
    If multiple ORFs of the same length are found, only the first
    (smaller start position) is reported.

    Args:
        sequence (str): DNA sequence in lower and/or uppercase.

    Returns:
        tuple[int, int, str]: Tuple of start position, length and
            peptide sequence. If no ORF was found, start position
            and length return values are -1. If `sequence` is an
            empty string, return an empty string for the peptide
            sequence.
    """
    [start,length] = find_longest_orf(sequence)

    if start != -1:
        string = translate_dna(sequence,start,length)
    else:
        string = ""

    return [start,length,string]
    
def valid_sequence(sequence):
  """Verifies that a nucleotide sequence contains only DNA letters.
  
  Valid characters include A, C, G, T and their lower-case variants.
  
  Args:
      sequence (str): Nucleotide sequence.
          
  Returns:
      bool: `True` if `sequence` contains only valid characters,
          `False` otherwise.  """

  valid_letters = ['A', 'C', 'G', 'T']
  
  for i in sequence.upper():
      if not i in valid_letters:
          return False
          
  return True

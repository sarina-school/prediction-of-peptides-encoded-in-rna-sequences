from chunk_sequence import chunk_sequence
from valid_sequence import valid_sequence

def translate_dna(sequence, start, length):
  """Translates DNA to the corresponding peptide sequence.

  Translates the DNA sequence starting at a specified position 
  (0-based) and extending a specified number of nucleotides 
  (length) into the corresponding peptide sequence, given the 
  universal genetic code.

  The genetic code is implemented as a dictionary 
  {“codon” : “AA”} (AA = amino acid in 1-letter code) and 
  includes entries for the stop codons, which are translated 
  to “*”, a non-amino acid character. 

  Args:
      sequence (str): DNA sequence in lower and/or uppercase.
      start (int): Sequence position/index from which to start
          nucleotide to amino acid translation.
      length (int): Number of nucleotides to translate.

  Returns:
      str: Peptide sequence corresponding to the indicated DNA
          sequence region. If the DNA sequence is an empty string,
          return an empty string.

      Raises:
          ValueError: `sequence` is not a valid DNA sequence.
          ValueError: `start` is out of bounds.
          ValueError: `length` is not a multiple of 3.
          ValueError: The sum of `start` and `length` - 1 goes
              beyond the end of the sequence.  """

  if not valid_sequence(sequence):
      raise ValueError("`sequence` is not a valid DNA sequence.")

  if start < 0 or start > len(sequence):
      raise ValueError("`start` is out of bounds.")
  
  if not length % 3 == 0:
      raise ValueError("`length` is not a multiple of 3.")

  if start + length - 1 > len(sequence):
      raise ValueError("The sum of `start` and `length` - 1 goes beyond the end of the sequence.")
  
  translation={"AAA": "K", "AAG": "K", "AAT":"N", "AAC":"N", "AGA": "R","AGG":"R","ATG": "M", "ACT": "T", "ACC":"T","ACA":"T","ACG" :"T", "AGT":"S", "AGC":"S", "AGA":"R", "AGG":"R", "ATT": "I", "ATC":"I","ATA":"I","CAT":"H", "CAC":"H", "CAA":"Q", "CAG":"Q", "CCT":"P", "CCC":"P", "CCA":"P", "CCG": "P","CGT":"R","CGC":"R","CGA":"R","CGG":"R", "CTT": "L","CTC":"L", "CTA":"L","CTG":"L","GAT":"D","GAC":"D","GAA":"E","GAG":"E", "GCT":"A","GCC":"A","GCA":"A","GCG":"A","GGT":"G","GGC":"G","GGA":"G","GGG":"G", "GTT":"V","GTC":"V", "GTG":"V","GTA":"V", "TAT": "Y","TAC":"Y", "TAA":"*","TAG":"*","TCT":"S", "TCC":"S", "TCA":"S","TCG":"S", "TGT":"C","TGC":"C","TGA":"*","TGG":"W","TTT":"F", "TTC":"F", "TTA":"L", "TTG":"L"}

  chunks = chunk_sequence(sequence, start)

  aminoacid = ""
  for item in chunks:
      aminoacid += translation[item]

      if translation[item] == "*":
          break
            
  return aminoacid
from valid_sequence import valid_sequence
from chunk_sequence import chunk_sequence

def find_start_codons(sequence):
  """searches all start condons 'ATG' and returns their indexes.

  Args:
      sequence (str): DNA sequence in lower and/or uppercase.
  
  Returns:
      array[dict{'start': idx}]: start index of the possible frames    """

  starts = []
  startIdx = -1
  charIdx=0
  while charIdx < len(sequence):
      charIdx = sequence.find('ATG', charIdx)

      if charIdx == -1:
          break

      startIdx = startIdx+1
      starts.append({})
      starts[startIdx]={'start': charIdx}
      charIdx += 3
  
  return starts

def find_longest_orf(sequence):
  """Reports longest open reading frame in a DNA sequence.

  Returns the start position (0-based) and length (in nucleotides) of
  the longest open reading frame (ORF) in the input sequence. Each
  ORF is delimited by a start (ATG) and a stop (TAA, TAG, TGA)
  codon. If there are multiple equivalent solutions, the one with the
  smallest start position/index is reported.

  Args:
      sequence (str): DNA sequence in lower and/or uppercase.
  
  Returns:
      tuple[int, int]: Start position/index and length of the longest
          open reading frame. If not ORF was found, return -1 for
          each.

  Raises:
      ValueError: `sequence` is not a valid DNA sequence.    """

  if not valid_sequence(sequence):
      raise ValueError(' `sequence` is not a valid DNA sequence')
  
  #find all start codons
  starts = find_start_codons(sequence)

  #iterate thorugh each start
  for startIdx in range(len(starts)):

      #split sequence beginning on current starts location
      chunks = chunk_sequence(sequence,starts[startIdx]['start'])

      #find end of this frame
      for i in range(len(chunks)):
          if chunks[i] in ['TAA','TAG','TGA']:
              #end of frame
              starts[startIdx]['length'] = i*3
              break
      
      if 'length' not in starts[startIdx]:
          #start has not been terminated
          starts[startIdx]['length'] = -1


  #find longes frame
  maxLength = -1
  maxIdx = -1
  for i in range(len(starts)):

      if starts[i]['length'] > maxLength:
          maxLength = starts[i]['length']
          maxIdx = starts[i]['start']
  
  return [maxIdx,maxLength]
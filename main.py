import sys
from parse_sequences import parse_sequences
from predict_orf_seq import predict_orf_seq

def main(fasta):
    """Predicts peptide sequences for open reading frames in input file.
    
    Given a FASTA file of DNA sequences, for each sequence, the longest
    open reading frame (ORF) is identified and the corresponding
    peptide sequence predicted.
    
    Results are printed to the screen in the following comma-separated
    format:
    
    sequence_name,index_start_orf,length_orf,peptide_sequence
    
    where
    
    sequence_name: is taken from the identifier line of each record in
        the input FASTA file
    index_start_orf: defines the 0-based index where the longest ORF
        was found
    length_orf: defines the length of the longest ORF in nucleotides
    peptide_sequence: is the peptide sequence (1-letter code) predicted
        to be translated from that ORF according to the universal
        genetic code

    An example output record might look like this:
    
    ABC1,0,18,MSEYQP*
    
    Args:
        fasta(str): Path to a FASTA file of DNA sequences.
    """
    [identifiers,sequences] = parse_sequences(fasta)

    for i in range(len(sequences)):
        [start,length,string] = predict_orf_seq(sequences[i])
        print(f"{identifiers[i]},{start},{length},{string}")

if __name__ == "__main__":
    #check for filename parameter
    if len(sys.argv) > 1:
        filename = sys.argv[1]
    else:
        filename = 'seq.fa' 
    
    main(filename)

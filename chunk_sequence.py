def chunk_sequence(sequence, offset=0):
  """Splits sequences in chunks of three letters starting
      at offset

  Args:
      sequence (str): DNA sequence in lower and/or uppercase.
      offset (int): Character offset (default: 0)
  
  Returns:
      array[str]: chunks of sequences  """

  return [sequence[0+i:3+i] for i in range(offset, len(sequence), 3)]

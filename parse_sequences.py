def parse_sequences(filename):
    """opens the file and splits the identifiers and the rest of the sequence
    Returns:
    a list with all the sequence_name and the sequence ifself"""

    file = open(filename, 'r')
    lines = file.readlines()

    identifiers = []
    sequences = []

    sequenceCount = 0
    for line in lines:

        if line.startswith('>'):
            #it is the identifier
            line=line.split(" ", 1)
            line="".join(line[0])
            identifiers.append(line.replace('>','').replace('\n',''))
            sequenceCount += 1
        else:
            #it is a sequence
            line = line.upper()
            
            if sequenceCount > len(sequences):
                sequences.append('')
            
            sequences[sequenceCount-1] += line.replace('\n','')
    
    return [identifiers,sequences]
